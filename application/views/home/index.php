<!DOCTYPE HTML>
<html class="<?php echo ($this->router->fetch_class()=='')?'home':$this->router->fetch_class(); ?>_<?php echo ($this->router->fetch_method()=='')?'index':$this->router->fetch_method(); ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TITLE</title>

<?php /* Stylesheets - Start */ ?>
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('bootstrap/css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('bootstrap/css/bootstrap-reboot.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('fontawesome/css/all.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('select2/css/select2.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('slick/slick.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('fancybox/jquery.fancybox.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('css/animate.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('css/stylesheet.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('css/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo assets_url('css/responsive.css'); ?>" />
<!-- new sweet alert -->
<link rel="stylesheet" type="text/css" href="<?php echo vendors_url('sweetalert/sweetalert.css'); ?>" />
<?php /* Stylesheets - End */ ?>

</head>

<body>

<?php /* #container - Start */ ?>
<div id="container" class="container-fluid">
    <div class="container">TEST</div>
</div>
<?php /* #container - End */ ?>

<?php /* Javascripts - Start */ ?>
<script type="text/javascript">
    var base_url = '<?php echo base_url(''); ?>';
</script>
<script type="text/javascript" src="<?php echo assets_url('js/jquery-3.4.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/jquery-migrate-1.4.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('lazyload/jquery.lazyload.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('select2/js/select2.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('slick/slick.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('fancybox/jquery.fancybox.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('isotope/isotope.pkgd.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('isotope/imagesloaded.pkgd.js'); ?>"></script>
<script type="text/javascript" src="<?php echo vendors_url('lazyload/jquery.lazyload.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/function.js'); ?>"></script>
<script src="<?php echo vendors_url('sweetalert/sweetalert.js'); ?>"></script>
<script src="<?php echo vendors_url('sweetalert/sweetalert.min.js'); ?>"></script>
<?php /* Javascripts - End */ ?>

</body>
</html>